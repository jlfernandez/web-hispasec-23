#!/bin/bash

# Getting current branch name
branch_name=$(git symbolic-ref -q HEAD)
branch_name=${branch_name##refs/heads/}
branch_name=${branch_name:-HEAD}

# Pulling new code
echo 'Retrieving '$branch_name
git reset --hard; git clean -f # Remove posible garbage
git pull origin $branch_name --quiet

sudo /home/develop/.virtualenvs/web/bin/pip install -r requirements.txt

sudo /home/develop/.virtualenvs/web/bin/pybabel -v extract -F babel.config -o ./locale/messages.pot ./templates/
sudo /home/develop/.virtualenvs/web/bin/pybabel compile -f -d locale/

sudo /home/develop/.virtualenvs/web/bin/pybabel update -l en_US -d ./locale -i ./locale/messages.pot
sudo /home/develop/.virtualenvs/web/bin/pybabel update -l es_ES -d ./locale -i ./locale/messages.pot

sleep 1

mkdir output
mkdir output/resources
mkdir output/static

sudo cp -r static/. output/static
sudo cp -r resources/. output/resources

sudo /home/develop/.virtualenvs/web/bin/python script.py

sudo cp templates/robots.txt output/robots.txt
sudo cp output/en/404.html output/404.html

# Reload apache
echo 'Reloading'
sudo service nginx reload
